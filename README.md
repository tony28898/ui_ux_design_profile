# UI/UX Design profile

## Project 1 : CustomerRewards Application design.
The app is specifically designed to address a business case challenge by providing users with the ability to effortlessly locate their desired products within the store, in addition to offering customer loyalty functionalities.

Check the full working Figma prototype [here](https://www.figma.com/proto/WBheAMhKYWywgMR5byPOEe/CustomerRewards-Application-design?page-id=0%3A1&type=design&node-id=2-3&viewport=105%2C277%2C0.39&t=130A1OSGJCplDlOx-1&scaling=scale-down&starting-point-node-id=2%3A3&show-proto-sidebar=1&mode=design).

## Screenshots
<img src="https://i.ibb.co/54zzwRs/Signin.png" width="200">
<img src="https://i.ibb.co/rx0mNNC/Stonks.png" width="200" >
<img src="https://i.ibb.co/kchqFCn/Product-selection.png" width="200">
<img src="https://i.ibb.co/vQ4VnB4/Product-selection-2.png" width="200">
<img src="https://i.ibb.co/27YGVTp/Notification.png" width="200">
<img src="https://i.ibb.co/yRjvnvy/Location.png" width="200">
<img src="https://i.ibb.co/6NTqCCs/Location-1.png" width="200">

## Project 2 : Passenger load-balancing solution
The primary objective of this app is to provide users with the convenience of locating the nearest train station with minimal congestion, while also offering enticing incentives as an added benefit. It was done within a tight timeframe of 24 hours.
Check the full working Figma prototype [here](https://www.figma.com/proto/3Ai5O9STHvTc1dvkj0MggZ/Smart-City-Hackathon-(Copy)?page-id=0%3A1&type=design&node-id=45-2&viewport=-963%2C460%2C0.22&t=CZEgchmjRNXgbeJh-1&scaling=scale-down&starting-point-node-id=45%3A2&mode=design).

## Screenshots
<img src="https://i.ibb.co/py5xCNj/Home.png" width="200">
<img src="https://i.ibb.co/xmNrWkV/Search.png" width="200" >
<img src="https://i.ibb.co/jrLxLyv/Map.png" width="200">
<img src="https://i.ibb.co/19f4d8d/Map-1.png" width="200">
<img src="https://i.ibb.co/fqdpcm6/Home-1.png" width="200">

